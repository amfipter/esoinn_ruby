class Selftest
  def initialize()
    @test1_samples = [
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 1, 1, 1, 0, 0, 0, 0, 0],
      [1, 1, 1, 1, 1, 0, 0, 0, 0],
      [0, 1, 1, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 1, 1, 0],
      [0, 0, 0, 0, 0, 1, 1, 1, 1],
      [0, 0, 0, 0, 1, 1, 1, 1, 1],
      [0, 0, 0, 0, 0, 1, 1, 1, 0],
      [0, 0, 0, 0, 0, 0, 1, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [1, 1, 1, 1, 0, 0, 0, 0, 0],
      [0, 0, 1, 1, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 1, 1, 0],
      [0, 0, 0, 0, 0, 1, 1, 1, 1]
    ]

    @test1_data = [
      [0, 1, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 1, 1, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 1, 0, 0],
      [0, 0, 0, 0, 0, 1, 1, 1, 1]
    ]
  end

  def data_test1()
    test_subject = ESOINN.new
    test_subject.first_init(@test1_samples.pop, @test1_samples.pop)
    10.times do
      @test1_samples.each do |i|
        puts '============================='
        test_subject.new_data(i)
      end
    end
    clusters, prototypes = test_subject.classify()
    puts "clusters: #{clusters}"
    puts "prototypes: #{prototypes}"
  end

  def cluster_search_test()
    cluster_search_test1(10)
    cluster_search_test1(100)
    cluster_search_test1(1000)
    cluster_search_test2(10)
    cluster_search_test2(100)
    cluster_search_test2(1000)
    nil
  end

  def parse_input()
    @raw_data = Array.new
    @all_data = Array.new
    File.open("profile") do |file|
      while(f = file.gets)
        f = f.split ' '
        f.map! {|el| el.to_i}
        @raw_data.push f[35..70]
      end
    end
    @data = Array.new(@raw_data[0].size)
    @raw_data[0].size.times do |i|
      print "\rPARSE: #{i+1}/#{@raw_data[0].size}"
      @data[i] = Array.new
      @raw_data.each do |el|
        j = i + @raw_data[0].size
        # puts el[i-2..i+2].to_s
        # puts el.to_s
        # sleep 1/5
        @data[i].push (el+el+el)[j-2..j+2]
      end
    end
    puts ""
    @data.each {|el| @all_data += el}
    @all_data.uniq!
  end

  def profile_test()
    parse_input()
    data = @all_data
    test_subject = ESOINN.new
    test_subject.first_init(data.pop, data.pop)
    i = 1
    data.each do |el|
      print "\r#{i}/#{data.size}"
      test_subject.new_data(el)
      i += 1
      #break if i == 10000
    end
    clusters, prototypes = test_subject.classify()
    
    puts "prototypes: "
    prototypes.keys.each do |key|
      puts prototypes[key].to_s
    end
    puts "clusters: #{clusters}"
  end

  def cluster_search_test1(count)
    print "cluster_search_test1 with #{count} vertices...\t"
    test_subject = ESOINN.new

    test_array = init_linked_vertices(count)

    test_subject.A = test_array
    test_subject.resolve_edge_state
    test_subject.update_id_cache

    #puts test_array.keys

    test_asw = Array.new

    test_array.keys.each do |i|
      test_asw.push i.id
    end

    test_asw.sort!

    #puts "start esoinn test.."
    result = test_subject.find_cluster(test_array.keys[0])
    #puts "test ended"

    #puts result.sort
    if(test_asw.eql? result.sort!)
      puts "PASSED"
    else
      puts "FAILED"
    end
    nil
  end

  def cluster_search_test2(count)
    print "cluster_search_test2 with #{count} vertices...\t"

    test_subject = ESOINN.new
    test_graph1 = init_linked_vertices(count/2)
    test_graph2 = init_linked_vertices(count/2)
    test_graph = test_graph1.merge(test_graph2)

    test_subject.A = test_graph
    test_subject.resolve_edge_state
    test_subject.update_id_cache

    test_data1 = Array.new
    test_data2 = Array.new
    test_data = Array.new

    #puts test_graph1

    test_graph1.keys.each do |key|
      test_data1.push key.id
    end

    test_graph2.keys.each do |key|
      test_data2.push key.id
    end

    test_data = test_data1.sort! + test_data2.sort!

    subject_result1 = test_subject.find_cluster(test_graph1.keys[0]).sort
    subject_result2 = test_subject.find_cluster(test_graph2.keys[0]).sort

    if(subject_result1.eql? test_data1)
      print "PASSED\t"
    else
      print "FAILED\t"
    end

    if(subject_result2.eql? test_data2)
      puts "PASSED"
    else
      puts "FAILED"
    end



    nil
  end

  def init_linked_vertices(count)
    #puts "INIT"
    test_array = Hash.new

    count.times do
      v = Vertex.new
      test_array[v] = Edges.new(v.id)
    end

    test_array.keys.each do |key|
      next if key.eql? test_array.keys[0]
      test_array[test_array.keys[0]].add(key.id)
    end

    test_array.keys.each do |key|
      next if key.eql? test_array.keys[0]
      Random.rand(0..test_array.keys.size - 1).times do
        test_array[key].add(test_array.keys[Random.rand(1..test_array.keys.size-1)].id)
      end
    end
    #puts "INIT END"
    test_array
  end

end
