class ESOINN
  attr_accessor :A

  def initialize()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @A = Hash.new
    @A_invert = nil
    @C = Array.new
    @l = 0                                             #new data iterator
    @clusters = nil                                    #contain all clusters
    @clusters_dominators = nil                         #contain dominator vertices in cluster
    @clusters_max_density = nil
    @dominators = nil
    @clusters_mean_density = nil
    @prototype_vectors = nil
    @mark_mean_density = nil
    @mark_max_density = nil
    @mark_cache = nil
    @data_size = nil
    nil
  end

  #complete
  def first_init(e1, e2)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    e = Vertex.new
    e.first_init(e1)
    @A[e] = Edges.new(e.id)
    e = Vertex.new
    e.first_init(e2)
    @A[e] = Edges.new(e.id)
    update_id_cache()
    @data_size = e1.size
    @l += 2
    nil
  end

  #complete
  def classify()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    update_all()
    update_cluster_cache()
    update_prototype_vectors()
    # @A.keys.each do |key|
    #   puts key
    # end
    return @clusters.size, @prototype_vectors
  end

  #progress => lambda_update
  def new_data(data)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @l += 1

    if(@A.size < 2)
      e = Vertex.new
      e.first_init(data)
      @A[e] = Edges.new(e.id)
      update_id_cache()
      return nil
    end

    best1, best2 = find_best(data)
    distance1 = best1.distance(data)
    distance2 = best2.distance(data)

    distance = distance1
    distance = distance2 if distance2 > distance1

    t1 = find_T(best1)
    t2 = find_T(best2)

    t = t1
    t = t2 if t2 < t1 

    if(distance > t)
      el = Vertex.new
      el.first_init(data)
      @A[el] = Edges.new(el.id)
      update_id_cache()                             #NEW
      return nil
    end
    @A[best1].increment_all_age()

    #resolve connection
    need_connection = link?(best1, best2)
    if(need_connection)
      if(@A[best1].exist_link?(best2.id))
        @A[best1].reset_link(best2.id)
        @A[best2].reset_link(best1.id)
      else
        @A[best1].add(best2.id)
        @A[best2].add(best1.id)
      end
    else
      if(@A[best1].exist_link?(best2.id))
        @A[best1].remove_link(best2.id)
        @A[best2].remove_link(best1.id)
      end
    end

    best1.increase_saved_signals()

    update_density(best1.id)

    update_weight(best1.id, data)

    delete_old_edges()

    lambda_update(best1, best2)


    nil
  end

  #complete
  def lambda_update(best1, best2)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    puts @l if $DEBUG
    return nil if @l % $lambda != 0

    update_all()
    update_class_mark(best1, best2)
    delete_noise()
    update_all()

    nil
  end

  #complete
  def update_all()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    clean_caches()

    update_id_cache()
    update_cluster_cache()
    update_clusters_dominators_cache()
    clean_non_dominator_vertex_mark()
    update_dominator_mark()
    update_env_of_dominator_mark()
    update_empty_marks()

    update_clusters_max_density()
    update_clusters_mean_density()
    update_mark_mean_density()
    update_mark_max_density()
    nil
  end

  #complete
  def delete_noise()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    for_del = Array.new
    c = 0
    @A.keys.each do |v|
      c += v.h / @A.keys.size.to_f
    end

    @A.keys.each do |v|
      if(@A[v].count == 2 and v.h < $c1 * c)
        for_del.push v
      elsif(@A[v].count == 1 and v.h < $c2 * c)
        for_del.push v
      elsif(@A[v].count == 0)
        for_del.push v
      end
    end

    for_del.each do |v|
      delete_all_links_with_vertex(v.id)
      @A.delete v
    end

    nil
  end

  #complete
  def get_prototype_vector(mark_id)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    max_h = 0.0
    @mark_cache[mark_id].each do |el|
      max_h += find_vertex_by_id(el).h
    end  

    prototype = Array.new(@data_size) {0}
    @mark_cache[mark_id].each do |el|
      v = find_vertex_by_id(el)
      w = v.W
      h = v.h
      w.size.times do |i|
        prototype[i] += (h/max_h) * w[i]
      end
    end
    prototype
  end



  #complete
  def delete_old_edges()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @A.keys.each do |key|
      @A[key].delete_old
    end
    nil
  end

  #complete
  def delete_all_links_with_vertex(v_id)
    #debug_print_graph()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @A.keys.each do |key|
      @A[key].remove_link(v_id)
    end
    nil
  end

  #complete
  def update_prototype_vectors()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    update_mark_cache()
    puts @mark_cache if $DEBUG
    @prototype_vectors = Hash.new(@mark_cache.size)
    @mark_cache.keys.each do |key|
      @prototype_vectors[key] = get_prototype_vector(key)
    end
    nil
  end

  #complete
  def update_mark_cache()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @mark_cache = Hash.new
    @A.keys.each do |el|
      mark = el.mark
      # puts mark
      # puts @mark_cache[mark]
      @mark_cache[mark] = Array.new if @mark_cache[mark].nil?
      @mark_cache[mark].push el.id
    end
    nil
  end

  #check
  def update_weight(v_id, input_w)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    v = find_vertex_by_id(v_id)
    neighbors = @A[v].get_id_to
    v.update_weight(1.0/v.M, input_w)
    neighbors.each do |el|
      find_vertex_by_id(el).update_weight(1.0/(100*v.M), input_w)
    end
    nil    
  end

  #complete
  def update_cluster_cache()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @clusters = Array.new
    debug_print_graph()
    v_ids = @A_invert.keys.clone
    v_ids.each do |v_id|
      next if help_cluster_contain?(v_id)
      cluster = find_cluster(v_id)
      @clusters.push cluster
    end
    nil
  end

  #complete
  def update_clusters_dominators_cache()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @clusters_dominators = Array.new(@clusters.size)
    #puts @clusters.to_s
    @dominators = find_dominators()
    @dominators.sort { |a, b| find_vertex_by_id(b).h <=> find_vertex_by_id(a).h}
    @dominators.each do |el|
      place = help_find_cluster_by_vertex(el)
      if(place.nil?)
        puts "WRONG DOMINATOR'S CLUSTER".red
        next
      end
      @clusters_dominators[place] = Array.new if @clusters_dominators[place].nil?
      @clusters_dominators[place].push el
    end
    nil
  end

  #complete
  def update_clusters_mean_density()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @clusters_mean_density = Array.new(@clusters.size)
    h_avg = 0
    n = 0
    @clusters.size.times do |i|
      @clusters[i].each do |el|
        v = find_vertex_by_id(el)
        h_avg += v.h
        n += 1
      end
      h_avg = h_avg.to_f / n.to_f
      @clusters_mean_density[i] = h_avg
      h_avg = 0
      n = 0
    end
    nil
  end

  def update_mark_mean_density()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @mark_mean_density = Hash.new
    h_avg = Hash.new
    n = Hash.new
    @A.keys.each do |v|
      h_avg[v.mark] = 0 if h_avg[v.mark].nil?
      n[v.mark] = 0 if n[v.mark].nil?

      h_avg[v.mark] += v.h
      n[v.mark] += 1
    end

    h_avg.keys.each do |key|
      @mark_mean_density[key] = h_avg[key].to_f / n[key].to_f
    end
    nil
  end


  #check
  def update_class_mark(best1, best2)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    overlap = find_overlap()
    while(overlap.size > 0)
      target_pair = overlap.pop
      v1 = target_pair.pop
      v2 = target_pair.pop
      next if find_vertex_by_id(v1).mark == find_vertex_by_id(v2).mark
      coef1 = find_coef(find_vertex_by_id(v1).mark)
      coef2 = find_coef(find_vertex_by_id(v2).mark)
      min = best1.h
      min = best2.h if min > best2.h
      if(min > coef1 or min > coef2)
        #merge
        change_mark(find_vertex_by_id(v2).mark, find_vertex_by_id(v1).mark)
      else
        #delete link
        @A[find_vertex_by_id(v1)].remove_link(v2)
        @A[find_vertex_by_id(v2)].remove_link(v1)
      end

    end
    nil
  end

  #complete
  def change_mark(from, to)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @A.keys.each do |v|
      if(v.mark == from)
        v.mark = to
      end
    end
    nil
  end

  #complete
  def update_dominator_mark()    
  puts __method__.upcase.to_s.red if $DEBUG_TRACE                         #alg 1 stage 1
    @dominators.each do |el|
      if(find_vertex_by_id(el).mark.nil?)
        find_vertex_by_id(el).mark = $mark
        $mark += 1
      end
    end
    nil
  end

  #complete
  def update_env_of_dominator_mark()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @dominators.each do |el|
      env = @A[find_vertex_by_id(el)].get_id_to
      mark = find_vertex_by_id(el).mark
      env.each do |target_vertex_id|
        target_vertex = find_vertex_by_id(target_vertex)
        target_vertex.mark = mark unless target_vertex.nil?
      end
    end
    nil
  end

  #complete; possible endless loop!
  def update_empty_marks()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    empty_id = Array.new
    @A.keys.each do |key|
      empty_id.push key.id if key.mark.nil?
    end
    while(true) do
      break if empty_id.size == 0
      v = empty_id.shift
      puts v if $DEBUG
      mark = help_find_mark(v)
      unless(mark.nil?)
        find_vertex_by_id(v).mark = mark
      else
        empty_id.push v
      end
    end
    nil
  end

  #check
  def update_density(v)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    v = v.id if v.class.eql? Vertex
    update_mean_distance(v)
    v_obj = find_vertex_by_id(v)
    h = 1.0/(v_obj.M*(1.0 + v_obj.d))
    v_obj.h = h
    nil
  end

  #check
  def update_mean_distance(v)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    v = v.id if v.class.eql? Vertex
    d = 0.0
    m = 0
    cluster = find_cluster(v)
    cluster.each do |el|
      next if el == v
      m += 1
      d += find_vertex_by_id(v).distance(find_vertex_by_id(el).W)
    end
    d /= m
    find_vertex_by_id(v).d = d
    nil
  end


  #complete
  def update_clusters_max_density()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @clusters_max_density = Array.new(@clusters)
    @clusters_dominators.size.times do |i|
      max = -1
      puts @clusters_dominators.to_s if $DEBUG
      @clusters_dominators[i].each do |el|
        h = find_vertex_by_id(el).h
        max = h if h > max
      end
      @clusters_max_density[i] = max
    end
    nil
  end

  def update_mark_max_density()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @mark_max_density = Hash.new
    @A.keys.each do |v|
      @mark_max_density[v.mark] = v.h if @mark_max_density[v.mark].nil?
      @mark_max_density[v.mark] = v.h if @mark_max_density[v.mark] < v.h
    end
    nil
  end


  def find_coef(mark_id)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    #mean = @clusters_mean_density[mark_id]
    #max = @clusters_max_density[mark_id]
    mean = @mark_mean_density[mark_id]
    max = @mark_max_density[mark_id]
    alpha = nil
    if(3.0 * mean >= max and max > 2.0 * mean)
      alpha = 0.5
    elsif(2.0 * mean >= max)
      alpha = 0
    elsif(max > 3.0 * mean)
      alpha = 1.0
    end

    max * alpha
  end
        

  #complete; main dependence => help_dominator
  def find_dominators()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    out = Array.new
    @A_invert.keys.each do |v_id|
      out.push v_id if help_dominator?(v_id)
    end
    out
  end

  #complete
  def find_best(data)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    keys = @A.keys.clone
    keys.sort! { |a, b| a.distance(data) <=> b.distance(data) }

    return keys[0], keys[1]
  end

  #complete; recursive; main dependence => find_cluster_core
  def find_cluster(v)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    v = v.id if v.class.eql? Vertex
    cluster = Array.new
    cluster.push v
    cluster = find_cluster_core(cluster, v)
    cluster
  end

  #complete; data dependence => @A_invert must be up-to-date
  def find_vertex_by_id(id)
    #puts __method__.upcase.to_s.red if $DEBUG_TRACE
    return @A_invert[id] unless @A_invert.nil?
    out = nil
    @A.keys.each do |v|
      #puts v
      out = v if v.id == id
    end
    #puts "try to search #{id}; find #{out}"
    out
  end

  #check
  def find_T(node)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    nodes = @A.keys
    t = nil
    if(@A[node].size == 0)
      t = $MAX_INT
      nodes.each do |el|
        next if el.eql?(node)
        d = el.distance(node.W)
        t = d if t > d
      end
    else
      t = 0
      connections = @A[node]
      nodes.each do |el|
        next unless connections.exist_link? el
        d = el.distance(node.W)
        t = d if t < d
      end
    end
    t
  end

  #complete
  def link?(v1, v2)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    if(v1.new? or v2.new?)
      return true
    elsif(v1.mark ==  v2.mark)
      return true
    elsif(v1.mark != v2.mark)
      unless(@clusters.nil? or @clusters_dominators.nil? or @clusters_max_density.nil? or @clusters_mean_density.nil?)
        min_v = v1.h
        min_v = v2.h if min_v > v2.h
        if(min > find_coef(v1.id) or min > find_coef(v2.id))
          return true
        end
      end
    end
    false
  end

  #complete
  def resolve_edge_state()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @A.keys.each do |key|
      @A[key].get_id_to.each do |id|
        @A[find_vertex_by_id(id)].add key.id
      end
    end
    nil
  end

  #complete
  def update_id_cache()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @A_invert = Hash.new
    @A.keys.each do |key|
      @A_invert[key.id] = key
    end
    nil
  end

  #complete
  def clean_non_dominator_vertex_mark()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @A_invert.keys.each do |key|
      next if @dominators.member? key
      find_vertex_by_id(key).mark = nil
    end
    nil
  end

  #complete; recursive part
  def find_overlap()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    vertices = @A_invert.keys.clone
    marked = Array.new
    out = Array.new
    while(vertices.size > 0) do
      v_id = vertices.pop
      marked, out = find_overlap_core(marked,v_id, out)
      vertices -= marked
    end
    out
  end
private

  #complete
  def debug_print_graph()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @A.keys.each do |key|
      puts key.to_s + ' ' + @A[key].get_id_to.to_s if $DEBUG
    end
    nil
  end

  #check
  def clean_caches()
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @clusters = nil                                    #contain all clusters
    @clusters_dominators = nil                         #contain dominator vertices in cluster
    @clusters_max_density = nil
    @dominators = nil
    @clusters_mean_density = nil
    nil
  end

  
  #check; recursive part
  def find_overlap_core(marked, v_id, out, root=nil)
    #puts __method__.upcase.to_s.red if $DEBUG_TRACE
    v = @A[find_vertex_by_id(v_id)].get_id_to
    v.each do |el|
      next if marked.member?  el
      marked.push el
      unless(root.nil?)
        if(find_vertex_by_id(root).mark != find_vertex_by_id(v_id))
          t = Array.new
          t.push root
          t.push v_id
          out.push t 
        end
      end
      marked, out = find_overlap_core(marked, el, out, v_id)
    end
    return marked, out
  end


  #complete; recursive
  def find_cluster_core(arr, v)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    # $K += 1
    # puts $K
    #update_id_cache()
    #debug_print_graph()
    puts v if $DEBUG
    puts find_vertex_by_id(v) if $DEBUG
    puts @A.to_s.green if $DEBUG
    vs = @A[find_vertex_by_id(v)].get_id_to()
    vs.each do |el|
      unless(arr.member? el)
        arr.push el
        arr = find_cluster_core(arr, el)
      end
    end
    arr
  end
  
  #complete
  def help_cluster_contain?(v_id)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    #puts '----'
    out = false
    @clusters.each do |el|
      #puts el
      out = true if el.member? v_id
    end
    out
  end

  #complete
  def help_dominator?(v_id)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    out = true
    v = find_vertex_by_id(v_id)
    v_h = v.h
    env = @A[v].get_id_to()
    env.each do |el|
      out = false if find_vertex_by_id(el).h > v_h
    end
    out
  end

  #complete
  def help_find_cluster_by_vertex(v_id)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    @clusters.size.times do |i|
      return i if @clusters[i].member? v_id
    end
    nil
  end

  def help_find_mark(v_id)
    puts __method__.upcase.to_s.red if $DEBUG_TRACE
    mark = nil
    h = -1
    puts v_id if $DEBUG
    puts find_vertex_by_id(v_id) if $DEBUG
    puts @A[find_vertex_by_id(v_id)] if $DEBUG
    @A[find_vertex_by_id(v_id)].get_id_to.each do |el|
      target = find_vertex_by_id(el)
      next if target.mark.nil?
      if(target.h > h)
        h = target.h
        mark = target.mark
      end
    end
    mark
  end


end

class Vertex
  attr_reader :id, :W
  attr_accessor :h, :d, :M, :mark

  #complete
  def initialize()
    @W = nil
    @id = $N
    $N += 1
    @M = 0
    @h = 1
    @d = 0
    @new = true
    @class = nil
    @mark = nil
    nil
  end

  #complete
  def first_init(w)
    @W = w.clone
    nil
  end

  #complete
  def distance(w)
    return nil if w.size != @W.size
    out = 0.0
    w.size.times do |i|
      out += (w[i] - @W[i])**2
    end
    Math.sqrt(out)
  end

  #complete
  def update_weight(c, input_w)
    delta = Array.new
    @W.size.times do |i|
      delta.push c*(input_w[i] - @W[i])
    end

    @W.size.times do |i| 
      @W[i] += delta[i]
    end
    nil
  end

  #complete
  def increase_saved_signals()
    @M += 1
    nil
  end

  #complete
  def to_s
    'VERTEX: ' + @id.to_s + ' ' + @W.to_s + ' h: ' + @h.to_s
  end

  #complete
  def new?()
    @new
  end


  
end

class Edges
  attr_reader :to

  #complete
  def initialize(from)
    @from = from
    @to = Hash.new
    nil
  end

  #complete
  def add(to)
    return nil if to == @from
    @to[to] = 0
    nil
  end

  #complete
  def size
    @to.keys.size
  end

  #complete
  def increment_all_age()
    @to.keys.each do |key|
      @to[key] += 1
    end
    nil
  end

  #complete
  def exist_link?(to)
    @to.keys.member? to
  end

  #complete
  def reset_link(to)
    add(to)
    nil
  end

  #complete
  def remove_link(to)
    @to.delete to
    nil
  end

  #complete
  def get_id_to()
    return @to.keys unless @to.keys.nil?
    Array.new
  end

  #complete
  def delete_old()
    old = Array.new
    @to.keys.each do |key|
      old.push key if @to[key] > $MAX_AGE
    end

    old.each do |el|
      @to.delete el
    end
    nil
  end

  #complete
  def to_s
    "EDGE from #{@from} to: #{@to.keys.to_s}"
  end

  #complete
  def count
    @to.keys.size
  end
end

