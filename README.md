# ESOINN implementation 

Contains:

+ `main.rb` ESOINN parameters 
+ `esoinn.rb` ESOINN implementation 
+ `selftest.rb` contains class for testing NN
+ `esoinn.graphml` represents structure of `esoinn.rb`

