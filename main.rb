#!/usr/bin/env jruby

require './esoinn.rb'
require './selftest.rb'
require 'colorize'

$MAX_INT = 2 ** 64
$MAX_AGE = 100
$N = 0
$K = 0
$lambda = 50
$mark = 0
$c1 = 0.0001
$c2 = 1.0

$DEBUG_TRACE = nil
$DEBUG = nil

test = Selftest.new
#test.cluster_search_test()
test.profile_test()